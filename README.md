![Still Another Asteroid Shooter](promo/feature_graphic.png)

A simple asteroid shooter game.

## How to Play
Tilt your device to dodge the asteroids. Tap to shoot them. Collect stars to recharge your laser. You can see how many shots you have in the top left corner.

## Bugs and Feature Requests
If you find a bug, please file an issue in this repository. Be specific about what is going wrong.

If you have a feature request, also file an issue. Be specific about what you'd like to see in the game. Note that I don't have a whole lot of time to work on new features; the game is intended to be simple, and I'm not going to add complex new mechanics.

Remember, Still Another Asteroid Shooter is open source. If you want to fork it and add your own crazy new ideas, go for it!

## License

The code for Still Another Asteroid Shooter is licensed under the MIT License. See `LICENSE.md`.

Public domain game art and sound effects from <https://kenney.nl>.

Font is VT323, by Peter Hull, downloaded from Google Fonts. Licensed under the Open Font License.

Soundtrack: "Captain Scurvy" Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 3.0 License
<http://creativecommons.org/licenses/by/3.0/>
