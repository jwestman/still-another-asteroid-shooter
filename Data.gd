extends Object

const PATH = "user://data.cfg"

var cfg

var high_score setget set_high_score, get_high_score
func set_high_score(newval):
	cfg.set_value("stats", "high_score", newval)
	cfg.save(PATH)
func get_high_score():
	return cfg.get_value("stats", "high_score", 0)

var stars setget set_stars, get_stars
func set_stars(newval):
	cfg.set_value("stats", "stars", newval)
	cfg.save(PATH)
func get_stars():
	return cfg.get_value("stats", "stars", 0)

var asteroids setget set_asteroids, get_asteroids
func set_asteroids(newval):
	cfg.set_value("stats", "asteroids", newval)
	cfg.save(PATH)
func get_asteroids():
	return cfg.get_value("stats", "asteroids", 0)

var music setget set_music, get_music
func set_music(newval):
	cfg.set_value("settings", "music", newval)
	cfg.save(PATH)
func get_music():
	return cfg.get_value("settings", "music", true)

func _init():
	cfg = ConfigFile.new()
	cfg.load(PATH)